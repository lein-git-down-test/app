(defproject app "0.0.0-SNAPSHOT"
  :dependencies [[org.clojure/clojure "1.10.1"]
                 [lib-a "master"]
                 [lib-b "master"]
                 [lib-c "master"]]
  :plugins [[reifyhealth/lein-git-down "0.4.0"]]
  :middleware [lein-git-down.plugin/inject-properties]
  :repositories [["gitlab" {:url "git://gitlab.com"}]]
  :git-down {lib-a {:coordinates lein-git-down-test/lib-a}
             lib-b {:coordinates lein-git-down-test/lib-b}
             lib-c {:coordinates lein-git-down-test/lib-c}})
